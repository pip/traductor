import pyGoogleTranslate
import sys
import re
pyGoogleTranslate.browser('firefox')

def procesar_archivo(archivo,lang):
    with open(archivo) as f:
        text = f.read().split('\n')
        text = [i for i in text if i]
    parrafos = []
    for p in text:
        parrafos.append(pyGoogleTranslate.translate(p,lang))
    traducido = '\n'.join(parrafos)
    return traducido

if len(sys.argv) == 3:
    archivo = sys.argv[1]
    lang = sys.argv[2]
    traduccion = procesar_archivo(archivo, lang)
    nombre_nuevo = 'traduc_' + archivo  
    with open(nombre_nuevo,'w') as f:
        f.write(traduccion)
else:
    print("Por favor introducir nombre de un archivo e idioma al que quiere traducirlo.")

        
