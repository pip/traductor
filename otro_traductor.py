#!/usr/bin/python
# coding: utf-8


import sys
import pyGoogleTranslate

archivo = sys.argv[1]

def procesa_archivo(archivo):
    original = leer_archivo(archivo)
    iniciar_api()
    traduccion = pyGoogleTranslate.translate(original, source_language='en', destination_language='es')
    cerrar_api()
    return traduccion


def leer_archivo(archivo):
    with open(archivo, 'rt') as a:
        original = a.read()
    return original

def iniciar_api():
    pyGoogleTranslate.browser('chrome')

def cerrar_api():
    pyGoogleTranslate.browser_kill()


def guardar_traduccion(traduccion, archivo):
    archivo_trad = archivo.replace('.', '_trad.')
    with open(archivo_trad, 'wt') as traducido:
        traducido.write(traduccion)


if not archivo:
    raise Exception('Falta el nombre del archivo a traducir')
else:
    traduccion = procesa_archivo(archivo)
    guardar_traduccion(traduccion, archivo)

