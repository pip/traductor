#!/usr/bin/python
# coding: utf-8

# convierte formato vtt a txt plano

import sys

argumento = sys.argv[1]


def contenido(nombre):
    with open(nombre, 'rt') as archivo:
        return archivo.read()

def inicio(contenido):
    primer_sub = contenido.find(' --> ')
    return contenido.rfind('\n', 0, primer_sub) + 1

def separar(contenido):
    return contenido.split('\n')

def texto_integrado(contenido):
    texto_final = ''
    for c in contenido:
        if not ' --> ' in c and c != '':
            if '...' in c:                # extrae los puntos suspensivos
                c = c.replace('...', '')
            texto_final += c + ' '
    return texto_final

def guarda_texto(contenido, nombre):
    nuevo_nombre = renombrar(nombre)
    with open(nuevo_nombre, 'wt') as archivo:
        archivo.write(contenido)

def renombrar(nombre):
    return nombre.replace('.vtt', '.txt')

if not argumento:
    raise Exception('Falta nombre de archivo.')

contenido = contenido(argumento)
guarda_texto(texto_integrado(separar(contenido[inicio(contenido):])), argumento)
