# Traductor Automágico

> Proyecto de la barca PyData

Vamos a armar un código loco que procese un archivo de texto (pista: el nombre
del archivo podría ser un argumento que recibe desde la terminal). La idea es
procesar cada párrafo por separado y dejarlos intercalados: los originales con
sus traducciones. Así luego es posible ir leyendo y corrigiendo lo que hiciese
falta... este proceso es el que realiza la barca de [Utopias
Piratas](https://utopias.partidopirata.com.ar/) en su proceso editorial
autónomo/ independiente.

## Ejercicios

1. Leer este pad hasta el final para tener andando todo y entender bien lo
   siguiente: Instalar con `pipenv update` en esta carpeta <3
1. Descargar un archivo markdown en ingles para pruebas: `$ wget
   https://0xacab.org/reginazabo/cyphersex/-/raw/master/pages/Eve.md -O Eve.md`
1. Agregarle funcionalidad al script para que pueda recibir el nombre del
   archivo desde la terminal. Pista: `print(sys.argv[0])`.
1. Programar la funcion `procesa_archivo()` que reciba un argumento
   `archivo_txt` y pueda cargar el archivo separando los parrafos. Probar algo
   minimo, quizas imprimir solo el primer parrafo. Usar este archivo o `Eve.md`
   como ejemplo.
1. Agregarle funcionalidad al script para que pueda recibir el nombre del
   archivo desde la terminal con import os/ sys/ argparse/ fire?
1. Agregarle funcionalidad para que traduzca cada parrafo. Guardar los parrafos
   intercalados en una copia de `archivo_txt` que tenga en ppio. el mismo
   nombre y despues cambie por ahi la extension o algo asi.
1. Programar una funcion auxiliar que recorte los parrafos en un largo de linea
   maximo.
1. Sumar esa funcionalidad.
1. Probar si la funcion auxiliar funciona bien con las sintaxis multilineas,
   tipo citas `>` e itemizado como `- + *`.

## Setup: Instalación Previa

1. Instalar pip `pip install --user --upgrade pip`
2. Refrescar la _shell_ para recargar la variante de ambiente `$PATH` (cerrando y abriendo terminal). Confirmar con `which pip`.
3. Instalar [pipenv](https://pipenv.pypa.io/en/latest/) con `pip install --user --upgrade pipenv`. Repetir paso anterior confirmando con `which pipenv`.

Si durante este tercer paso aparece:

> Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.

Le hacemos:

```bash  
echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.bash_profile
```

Esto agrega en nuestro "PATH" la ruta donde se encuentran los programitos que instalamos con pip.

4. Probar que se instaló correctamente `pipenv -h` y se listan sus subcomandos. Los mas importantes van a ser: `install`, `update` y `shell`.

## Ambientes Virtuales

- Elije Conda ó Pipenv:

### Anaconda

> Sirve para Windows, Mac, Mint, etc.

1. Bajarlo de anaconda.org
2. `chmod +x ./Anaconda*.sh`
3. `./Anaconda*.sh`
4. Barra espaciadora hasta llegar al final del texto. Escribir "yes".
5. Despues pregunta donde queremos tener anaconda y sus ambientes virtuales (no podremos mover la carpeta mas tarde!!). Por defecto es ~/anaconda3/ o ~/miniconda3/.
7. Instalamos libreria en ambiente virtual. Lo haremos con "--name" en lugar de "--prefix", pero son dos opciones de cómo usar la herramienta.

```bash
conda env create --name traduccion psutil
conda activate traduccion
pip install pyGoogleTranslate
```

Luego de usar pip en el ambiente, ya no da usar conda para instalar algo en ese mismo ambiente, se va a romper tarde o temprano.

### Pipenv

1. Armar un ambiente virtual con `pipenv`:

```bash
pwd
mkdir traductor
cd traductor
git init
pipenv install pyGoogleTranslate
echo ".venv/" > .gitignore
pipenv shell
```

## pyGoogleTranslate

Leer su [web](https://pypi.org/project/pyGoogleTranslate/). Instalar `geckodriver` o `chrome` con el administrador de paquetes del sistema operativo. No funciona con `chromium`.

Probar que funciona, pegar esto en una shell de python.

```python
import pyGoogleTranslate
pyGoogleTranslate.browser('chrome')  # poner 'firefox' para usar geckodriver
pyGoogleTranslate.translate('Hello', 'ja')
```

### Thonny IDE

Yendo a "settings", la solapa "interpreter" permite elegir la opcion "alternate interpreter or virtual environment" y luego aparece un boton para elegir la ruta del ambiente virtual sea de conda o pipenv, asi podemos trabajar con las librerias como pyGooTranslate.

