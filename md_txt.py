#!/usr/bin/python
# coding: utf-8

# extrae el texto plano del formato '.md'
# no reconoce etiquetas <html>

import sys

archivo = sys.argv[1]


def leer_archivo(archivo):
    with open(archivo, 'rt') as a:
        original = a.read()
    return original


def recomponer_parrafos(texto_marcado):

    texto_plano = ''
    posicion = 0
    cita = False

    while posicion < len(texto_marcado)-1:
        if texto_marcado[posicion] != '\n':
            if texto_marcado[posicion] != '>':
                texto_plano += texto_marcado[posicion]
                posicion += 1
            else:
                if cita == False:
                    texto_plano += chr(171) + texto_marcado[posicion+2]
                    posicion += 3
                    cita = not cita
                else:
                    texto_plano += texto_marcado[posicion+1]
                    posicion += 2
        else:
            if texto_marcado[posicion+1] != '\n' and texto_marcado[posicion+1] != '>' and cita != True:
                texto_plano += ' '
                posicion += 1
            elif texto_marcado[posicion+1] == '>' and texto_marcado[posicion+2] == '\n' and cita == True:
                texto_plano += '\n' + '\n'
                posicion += 2
            elif texto_marcado[posicion+1] == '>' and texto_marcado[posicion+2] == ' ' and cita == True:
                texto_plano += ' '
                posicion += 3
            elif texto_marcado[posicion+1] != '>' and texto_marcado[posicion+1] == '\n' and cita == True:
                texto_plano += chr(187) + '\n\n'
                cita = not cita
                posicion += 2
            elif texto_marcado[posicion+1] != '>' and cita == True:
                texto_plano += chr(187) + ' '
                cita = not cita
                posicion += 1
            else:
                contador = 0
                while texto_marcado[posicion+contador] == '\n':
                    contador += 1
                for c in range(contador-1):
                    texto_plano += '\n'
                posicion += contador

    return texto_plano


original = leer_archivo(archivo)
print(recomponer_parrafos(original))
